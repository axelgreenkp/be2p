#!/bin/bash
echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: kill all" >> /var/log/be2p/log
kill $(ps aux | grep 'tail_.*.s[h]' | awk '{print $2}')
