#!/bin/bash
tail --pid=$$ --follow=name --retry --silent --max-unchanged-stats=5 --quiet --sleep-interval=0.1 $1 | sed -n -r "s/^(.{19}) (.{6}-.{6}-.{2}) \*\* [^@]+@[^ ]+ (R=dnslookup T=remote_smtp: )?(.*)$/\2	\1	\4/p" --unbuffered >> ${2}bounce_output
