#!/bin/bash
CURRENT_PATH="$( cd "$( dirname $0 )" && pwd )"
rm -R ${CURRENT_PATH}/.* /var/log/be2p
mkdir ${CURRENT_PATH}/.data ${CURRENT_PATH}/.failed_output ${CURRENT_PATH}/.logs /var/log/be2p
chmod 777 ${CURRENT_PATH}/.data ${CURRENT_PATH}/.failed_output ${CURRENT_PATH}/.logs /var/log/be2p -R
chmod +x ${CURRENT_PATH}/*.sh
chown root:root ${CURRENT_PATH}/*
chown postgres:postgres ${CURRENT_PATH}/psql_execute.sh
/bin/sh ${CURRENT_PATH}/tails_kill_all.sh
crontab -r
cp ${CURRENT_PATH}/crontab_template ${CURRENT_PATH}/new_crontab
sed -i "s%CURRENT_PATH%$CURRENT_PATH%g" ${CURRENT_PATH}/new_crontab
crontab ${CURRENT_PATH}/new_crontab
rm ${CURRENT_PATH}/new_crontab
/bin/sh ${CURRENT_PATH}/psql_execute.sh ${CURRENT_PATH}/db_template
/etc/init.d/cron restart
echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: initialization complete" >> /var/log/be2p/log
