#!/bin/bash
CURRENT_PATH="$( cd "$( dirname $0 )" && pwd )"
NEW_INFO=0

# start
FILE=$(${CURRENT_PATH}/file_rotate.sh ${CURRENT_PATH}/.data/start_output)
echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: start $FILE" >> /var/log/be2p/log
if [ $(wc -l $FILE | awk '{print $1}') -ne 0 ]; then
	sed -i '1s/^/COPY exim_logs (exim_id, send_id, queue_id, start_date) FROM STDIN;\n/' $FILE
	if [ $(${CURRENT_PATH}/psql_execute.sh $FILE 2>&1 | wc --chars) -eq 0 ]; then
	        NEW_INFO=$((NEW_INFO+1))
	        echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: succesfully saved $FILE" >> /var/log/be2p/log
	        rm $FILE
	else
	        echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: error while saving $FILE" >> /var/log/be2p/log
	        mv $FILE ${CURRENT_PATH}/.failed_output/
	fi
else
	rm $FILE
	echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: file $FILE is empty, so removed" >> /var/log/be2p/log
fi

# success
FILE=$(${CURRENT_PATH}/file_rotate.sh ${CURRENT_PATH}/.data/success_output)
echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: start $FILE" >> /var/log/be2p/log
if [ $(wc -l $FILE | awk '{print $1}') -ne 0 ]; then
	sed -i '1s/^/COPY exim_logs (exim_id, success_date) FROM STDIN;\n/' $FILE
	if [ $(${CURRENT_PATH}/psql_execute.sh $FILE 2>&1 | wc --chars) -eq 0 ]; then
		NEW_INFO=$((NEW_INFO+1))
		echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: succesfully saved $FILE" >> /var/log/be2p/log
	        rm $FILE
	else
		echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: error while saving $FILE" >> /var/log/be2p/log
	        mv $FILE ${CURRENT_PATH}/.failed_output/
	fi
else
        rm $FILE
        echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: file $FILE is empty, so removed" >> /var/log/be2p/log
fi

# bounce
FILE=$(${CURRENT_PATH}/file_rotate.sh ${CURRENT_PATH}/.data/bounce_output)
echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: start $FILE" >> /var/log/be2p/log
if [ $(wc -l $FILE | awk '{print $1}') -ne 0 ]; then
	sed -i '1s/^/COPY exim_logs (exim_id, bounce_date, bounce_message) FROM STDIN;\n/' $FILE
	if [ $(${CURRENT_PATH}/psql_execute.sh $FILE 2>&1 | wc --chars) -eq 0 ]; then
		NEW_INFO=$((NEW_INFO+1))
		echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: succesfully saved $FILE" >> /var/log/be2p/log
	        rm $FILE
	else
		echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: error while saving $FILE" >> /var/log/be2p/log
	        mv $FILE ${CURRENT_PATH}/.failed_output/
	fi
else
        rm $FILE
        echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: file $FILE is empty, so removed" >> /var/log/be2p/log
fi

if [ "$NEW_INFO" -gt "0" ]; then
	/bin/sh ${CURRENT_PATH}/log_flag_change.sh
	echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: log flag change send $FILE" >> /var/log/be2p/log
fi
