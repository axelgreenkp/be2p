#!/bin/bash
tail --pid=$$ --follow=name --retry --silent --max-unchanged-stats=5 --quiet --sleep-interval=0.1 $1 | sed -n -r "s/^(.{19}) (.{6}-.{6}-.{2}) == [^@]+@[^ ]+ .*R=dnslookup.* defer \((-?[0-9]+)\): (.*)$/\2	\1	\3	\4/p" --unbuffered >> ${2}error_output
