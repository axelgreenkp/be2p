#!/bin/bash
# initialize days parameter
DAYS=5
if [ $# -ne 0 ]; then
	if [ "$1" -gt "0" ]; then
		DAYS=$1
	fi
fi

# delete old logs
QUERY="DELETE FROM exim_logs WHERE update_date < NOW() - INTERVAL '$DAYS days';"
RESULT=$(su - postgres -c "psql -c \"$QUERY\"")
echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: drop logs - $RESULT" >> /var/log/be2p/log

#delete old errors
QUERY="DELETE FROM temp_errors WHERE create_date < NOW() - INTERVAL '$DAYS days';"
RESULT=$(su - postgres -c "psql -c \"$QUERY\"")
echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: drop errors - $RESULT" >> /var/log/be2p/log
