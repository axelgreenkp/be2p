#!/bin/bash
CURRENT_PATH="$( cd "$( dirname $0 )" && pwd )"
NEW_INFO=0
NEW_INFO_ERRORS=0

#failed start
for FILE in ${CURRENT_PATH}/.failed_output/start_output_*
do
	if [ $(${CURRENT_PATH}/psql_execute.sh $FILE 2>&1 | wc --chars) -eq 0 ]; then
		NEW_INFO=$((NEW_INFO+1))
		echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: successfully saved $FILE" >> /var/log/be2p/log
		rm $FILE
	fi
done

#failed success
for FILE in ${CURRENT_PATH}/.failed_output/success_output_*
do
	if [ $(${CURRENT_PATH}/psql_execute.sh $FILE 2>&1 | wc --chars) -eq 0 ]; then
		NEW_INFO=$((NEW_INFO+1))
		echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: successfully saved $FILE" >> /var/log/be2p/log
                rm $FILE
        fi
done

#failed bounce
for FILE in ${CURRENT_PATH}/.failed_output/bounce_output_*
do
        if [ $(${CURRENT_PATH}/psql_execute.sh $FILE 2>&1 | wc --chars) -eq 0 ]; then
		NEW_INFO=$((NEW_INFO+1))
		echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: successfully saved $FILE" >> /var/log/be2p/log
                rm $FILE
        fi
done

#failed error
for FILE in ${CURRENT_PATH}/.failed_output/error_output_*
do
        if [ $(${CURRENT_PATH}/psql_execute.sh $FILE 2>&1 | wc --chars) -eq 0 ]; then
		NEW_INFO_ERRORS=$((NEW_INFO_ERRORS+1))
		echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: successfully saved $FILE" >> /var/log/be2p/log
                rm $FILE
        fi
done

if [ "$NEW_INFO" -gt "0" ]; then
        /bin/sh ${CURRENT_PATH}/log_flag_change.sh
        echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: retry log flag change send $FILE" >> /var/log/be2p/log
fi

if [ "$NEW_INFO_ERRORS" -gt "0" ]; then
        /bin/sh ${CURRENT_PATH}/error_flag_change.sh
        echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: retry error flag change send $FILE" >> /var/log/be2p/log
fi

