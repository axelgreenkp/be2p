#!/bin/bash
CURRENT_PATH="$( cd "$( dirname $0 )" && pwd )"
if [ $(ps aux | grep "tail_start.s[h] $1" | wc -l) -eq 0 ]; then
	echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: run tail_start.sh" >> /var/log/be2p/log
	/bin/sh ${CURRENT_PATH}/tail_start.sh $1 $2 > /dev/null 2>&1 &
fi
if [ $(ps aux | grep "tail_success.s[h] $1" | wc -l) -eq 0 ]; then
	echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: run tail_success.sh" >> /var/log/be2p/log
	/bin/sh ${CURRENT_PATH}/tail_success.sh $1 $2 > /dev/null 2>&1 &
fi
if [ $(ps aux | grep "tail_bounce.s[h] $1" | wc -l) -eq 0 ]; then
	echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: run tail_bounce.sh" >> /var/log/be2p/log
        /bin/sh ${CURRENT_PATH}/tail_bounce.sh $1 $2 > /dev/null 2>&1 &
fi
if [ $(ps aux | grep "tail_error.s[h] $1" | wc -l) -eq 0 ]; then
	echo "$(date +'%Y-%m-%d %H:%M:%S.%6N') $0: run tail_error.sh" >> /var/log/be2p/log
        /bin/sh ${CURRENT_PATH}/tail_error.sh $1 $2 > /dev/null 2>&1 &
fi
